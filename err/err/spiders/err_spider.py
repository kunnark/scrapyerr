import os
import scrapy
from lxml import html
import time

class ERRSpider(scrapy.Spider):
    name = "err"
    dir = "./database/2019-06-28/"

    def start_requests(self):
        start_time = time.time()
        baseUrl = "https://err.ee/"

        # make dir
        if not os.path.isdir(self.dir):
            os.mkdir(self.dir)
        # begin, end - command line arguments
        for url in range(int(self.begin), int(self.end)):
            id = str(url)
            realUrl = baseUrl+id
            # print out link and time
            # print('{} | {}'.format(id, (time.time() - start_time)))

            # check if file exists
            if not os.path.isfile(self.dir+id+".txt"):
                yield scrapy.Request(url=realUrl, callback=self.parse)
        print("--- %s seconds ---" % (time.time() - start_time))

    def parse(self, response):
        # save final url
        if response.status in [200, 302]:
            url = response.url
        else:
            url = "nullValue"
        pageId = response.url.split("/")[-2]
        body = (response.body).decode(encoding="utf-8") # cast response to str
        article = self.getArticleDataAsDict(body, pageId, url)
        self.writeToFile(article, pageId)


    def getArticleDataAsDict(self, body, pageId, url):
        # data to crab
        # headline - article headline
        # lead
        # content - article content text
        # editor
        article = {
            "headline":"/html/body/div[1]/div[2]/div/div[1]/article/header/h1/text()",
            "lead":"//*[@class='lead']/p/text()",
            "content":"/html/body/div[1]/div[2]/div/div[1]/article/div/div[4]/p", # do not change
            "editor":"//*[@id='article']/div[1]/article/div/p/text()",
            "datePublished":"//time[@class='pubdate']/@datetime",
            "category":"//*[@id='article']/div[1]/article/div/div[1]/div[1]/section/div[1]/a/text()",
            "author":"//*[@id='article']/div[1]/article/div/div[1]/div[1]/section/div[2]/p/text()"
        }

        # Mapping xpath with content:
        tree = html.fromstring(body)
        response = {}
        for key, val in article.items():
            # editor, date, category, author, lead:
            valueList = tree.xpath(article[key])

            if valueList:
                value = valueList.pop(0)
                response[key] = str(value)
            else:
                response[key] = "nullValue"

            # content
            if(key == 'content'):
                # content
                count = (int)(tree.xpath("count(" + article['content'] + ")"))  # count of subitems <p> in content
                empty = ''
                content = ''
                for i in range(1, count + 1):
                    number = str(i)
                    empty = empty.join(tree.xpath(article['content'] + "[" + number + "]/text()"))
                    content += empty
                response['content'] = content

            # errID
            response['errID'] = pageId
            response['url'] = url
        return response

    def writeToFile(self, article, fileName):
        if "www" or ".ee" in fileName:
            pass
        if article["errID"] is None:
            pass
        if article["content"] is None or article["content"] is "":
            pass
        f = open(self.dir+fileName+".txt", "w+")
        for key, value in article.items():
            beginTag = "<" + key + ">"
            closingTag = "</" + key + ">"
            # write result
            f.write(beginTag+article[key]+closingTag+"\n")
        f.close()

