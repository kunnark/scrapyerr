# -*- coding: utf-8 -*-
import sqlalchemy as db
import time
import scrapy
from lxml import html
from urllib.parse import urlparse
import re
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, Sequence, DateTime, MetaData, Table
from sqlalchemy.ext.declarative import declarative_base
import logging
from sqlalchemy.sql import select
import timeout_decorator

# runspider:
# go to spider directory
# scrapy crawl err_related_news

class RelatedSpider(scrapy.Spider):
    logging.getLogger('scrapy').propagate = False
    name = 'err_related_news'
    begin = 1
    end = 1

    @timeout_decorator.timeout(1, use_signals=True)
    def urls_to_scrape(self):
        engine = db.create_engine('postgresql://127.0.0.1:5432/kunnark')
        connection = engine.connect()
        Session = sessionmaker()
        Session.configure(bind=engine)
        session = Session()
        metadata = MetaData()

        err_news_orig = Table('err_news_orig', metadata,
            Column('id', Integer, primary_key=True),
            Column('err_id', String),
            Column('related_urls', String),
        )

        s = select([err_news_orig])
        result = connection.execute(s)

        response_urls = []
        for row in result:
            list_of_urls = str(list(row)[2]).split(",")
            parent_err_id = str(list(row)[1])
            dict_url = {
                        'parent_err_id':parent_err_id,
                        'err_id':list_of_urls
                        }
            if 'nullValue' not in list_of_urls:
                response_urls.append(dict_url)
        session.close()
        connection.close()
        return response_urls

    @timeout_decorator.timeout(1, use_signals=True)
    def start_requests(self):
        baseUrl = "https://err.ee/"
        start_time = time.time()
        counter = 0
        urls = self.urls_to_scrape()

        for item in urls:
            dict_of_urls = item
            id = str(item)
            parent_err_id = item['parent_err_id']
            for err_id in item['err_id']:
                realUrl = baseUrl+err_id
                if counter%1000 == 0:
                    print('Scraped:'+str(counter))
                counter += 1
                request = scrapy.Request(url=realUrl, callback=self.parse)
                request.cb_kwargs['parent_err_id'] = parent_err_id
                yield request
        print("--- %s seconds ---" % (time.time() - start_time))

    def get_page_id_from_url(self, url) -> str:
        parsed = urlparse(url)
        path = parsed.path
        pattern = re.compile("[\/][0-9]+[\/]")
        page_id = str(pattern.findall(path).pop(0)).replace("/", "")
        return page_id

    def parse(self, response, parent_err_id):
        try:
            # save final url
            if response.status in [200, 302]:
                url = response.url

                # skip non-text news and rus and eng news:
                hostname = urlparse(url).hostname
                skipped_urls = ['rus', 'news', 'klassikaraadio', 'vikerraadio', 'etv', 'etv2', 'r2', 'lasteekraan']
                for s in skipped_urls:
                    if s in hostname:
                        return
            else:
                url = "nullValue"
            pageId = response.url.split("/")[-2]
            body = (response.body).decode(encoding="utf-8") # cast response to str
            article = self.getArticleDataAsDict(body, pageId, url)

            # write article to db
            engine = db.create_engine('postgresql://127.0.0.1:5432/kunnark')
            connection = engine.connect()
            Session = sessionmaker()
            Session.configure(bind=engine)
            session = Session()
            self.writeToDb(article, engine, session, parent_err_id)
        except timeout_decorator.TimeoutError:
            print("Sain vea.")

    @timeout_decorator.timeout(1, use_signals=True)
    def getArticleDataAsDict(self, body, pageId, url):
        # data to crab
        # headline - article headline
        # lead
        # content - article content text
        # editor

        # detect language by hostname
        language = "EST"

        article = {
            "headline":"//*[@id='ng-app']/body/div[1]/div[2]/div/div[1]/div[1]/div/article/header/h1/text()",
            "lead":"//*[@class='lead']/p/text()",
            "content":"//div[contains(@class, 'text')]/p", # do not change
            "editor":"//*[@id='article']/div[1]/article/div/p/text()",
            "datePublished":"//time[@class='pubdate']/@datetime",
            "category":"//*[@id='article']/div[1]/article/div/div[1]/div[1]/section/div[1]/a/text()",
            "author":"//span[contains(@class, 'name')]/text()",
            "same_topic_urls":"//div[contains(@class, 'kahene-v2ike')]/aside/a/@href",
            "language": "/html"
        }

        # Mapping xpath with content:
        tree = html.fromstring(body)
        response = {}
        for key, val in article.items():
            # editor, date, category, author, lead:
            valueList = tree.xpath(article[key])

            if valueList:
                # same topic urls
                if (key == 'same_topic_urls'):
                    temp_value_list = []
                    for url in valueList:
                        page_id = self.get_page_id_from_url(url)
                        temp_value_list.append(page_id)
                    value = ','.join(temp_value_list)
                    response[key] = str(value)
                else:
                    value = valueList.pop(0)
                    response[key] = str(value)
            else:
                if key == 'date_published':
                    response[key] = "1970-01-01 00:00:00+02"
                response[key] = "nullValue"

            # content
            if(key == 'content'):
                # content
                count = (int)(tree.xpath("count(" + article['content'] + ")"))  # count of subitems <p> in content
                empty = ''
                content = ''
                for i in range(1, count + 1):
                    number = str(i)
                    empty = empty.join(tree.xpath(article['content'] + "[" + number + "]/text()"))
                    content += empty
                response['content'] = content

            if(key == 'language'):
                response['language'] = language
            # errID
            response['errID'] = pageId
            response['url'] = url
        return response

    @timeout_decorator.timeout(1, use_signals=True)
    def writeToDb(self, data, engine, session, parent_err_id):
        metadata = db.MetaData()
        table = db.Table('err_news_orig', metadata, autoload=True, autoload_with=engine)
        article = Article(
            err_id = data['errID'],
            author = data['author'],
            related_urls = data['same_topic_urls'],
            url = data['url'],
            lead = data['lead'],
            content = data['content'],
            category = data['category'],
            date_published = data['datePublished'],
            editor = data['editor'],
            headline = data['headline'],
            language = data['language'],
            parent_err_id = parent_err_id
        )
        # Write only existing articles:
        article_written = [article.headline, article.lead, article.content]
        for a in article_written:
            if a == 'nullValue':
                return
            else:
                session.add(article)
                session.commit()

# article
Base = declarative_base()
class Article(Base):
    __tablename__ = 'err_news_related'
    id = Column(Integer, Sequence('err_news_related_id_seq'), primary_key=True)
    err_id = Column(Integer)
    author = Column(String)
    related_urls = Column(String)
    url = Column(String)
    lead = Column(String)
    content = Column(String)
    category = Column(String)
    date_published = Column(DateTime)
    editor = Column(String)
    headline = Column(String)
    language = Column(String)
    parent_err_id = Column(Integer)

    def __repr__(self):
        return "<Article(id='%s', headline='%s', err_id='%s')>" % (
                                self.id, self.headline, self.err_id)

class Url(Base):
    __tablename__ = 'err_news_orig'
    id = Column(Integer, Sequence('err_news_orig_id_seq'), primary_key=True)

    parent_err_id = Column(Integer)
    err_id = Column(Integer)

    def __repr__(self):
        return "<Article(parent_err_id='%s',  err_id='%s')>" % (
                                self.parent_err_id, self.err_id)