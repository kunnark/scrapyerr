import sqlalchemy as db
import time
import scrapy
from lxml import html
from urllib.parse import urlparse
import re
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, Sequence, DateTime
from sqlalchemy.ext.declarative import declarative_base
import logging

class NewsSpider(scrapy.Spider):
    logging.getLogger('scrapy').propagate = False
    name = 'NewsSpider'
    begin = 1010000
    end = 1020336
    def start_requests(self):
        baseUrl = "https://err.ee/"
        start_time = time.time()
        counter = 0
        for url in range(int(self.begin), int(self.end)):
            id = str(url)
            realUrl = baseUrl+id
            if counter%1000 == 0:
                print('Scraped:'+str(counter))
            counter += 1
            yield scrapy.Request(url=realUrl, callback=self.parse)

        print("--- %s seconds ---" % (time.time() - start_time))

    def get_page_id_from_url(self, url) -> str:
        parsed = urlparse(url)
        path = parsed.path
        pattern = re.compile("[\/][0-9]+[\/]")
        page_id = str(pattern.findall(path).pop(0)).replace("/", "")
        return page_id

    def parse(self, response):
        # save final url
        if response.status in [200, 302]:
            url = response.url

            # skip non-text news and rus and eng news:
            hostname = urlparse(url).hostname
            skipped_urls = ['rus', 'news', 'klassikaraadio', 'vikerraadio', 'etv', 'etv2', 'r2', 'lasteekraan']
            for s in skipped_urls:
                if s in hostname:
                    return
        else:
            url = "nullValue"
        pageId = response.url.split("/")[-2]
        body = (response.body).decode(encoding="utf-8") # cast response to str
        article = self.getArticleDataAsDict(body, pageId, url)

        # write article to db
        engine = db.create_engine('postgresql://127.0.0.1:5432/kunnark')
        connection = engine.connect()
        Session = sessionmaker()
        Session.configure(bind=engine)
        session = Session()
        self.writeToDb(article, engine, session)

    def getArticleDataAsDict(self, body, pageId, url):
        # data to crab
        # headline - article headline
        # lead
        # content - article content text
        # editor

        # detect language by hostname
        language = "EST"

        article = {
            "headline":"//*[@id='ng-app']/body/div[1]/div[2]/div/div[1]/div[1]/div/article/header/h1/text()",
            "lead":"//*[@class='lead']/p/text()",
            "content":"//div[contains(@class, 'text')]/p", # do not change
            "editor":"//*[@id='article']/div[1]/article/div/p/text()",
            "datePublished":"//time[@class='pubdate']/@datetime",
            "category":"//*[@id='article']/div[1]/article/div/div[1]/div[1]/section/div[1]/a/text()",
            "author":"//span[contains(@class, 'name')]/text()",
            "same_topic_urls":"//div[contains(@class, 'kahene-v2ike')]/aside/a/@href",
            "language": "/html"
        }

        # Mapping xpath with content:
        tree = html.fromstring(body)
        response = {}
        for key, val in article.items():
            # editor, date, category, author, lead:
            valueList = tree.xpath(article[key])

            if valueList:
                # same topic urls
                if (key == 'same_topic_urls'):
                    temp_value_list = []
                    for url in valueList:
                        page_id = self.get_page_id_from_url(url)
                        temp_value_list.append(page_id)
                    value = ','.join(temp_value_list)
                    response[key] = str(value)
                else:
                    value = valueList.pop(0)
                    response[key] = str(value)
            else:
                if key == 'date_published':
                    response[key] = "1970-01-01 00:00:00+02"
                response[key] = "nullValue"

            # content
            if(key == 'content'):
                # content
                count = (int)(tree.xpath("count(" + article['content'] + ")"))  # count of subitems <p> in content
                empty = ''
                content = ''
                for i in range(1, count + 1):
                    number = str(i)
                    empty = empty.join(tree.xpath(article['content'] + "[" + number + "]/text()"))
                    content += empty
                response['content'] = content

            if(key == 'language'):
                response['language'] = language
            # errID
            response['errID'] = pageId
            response['url'] = url
        return response

    def writeToDb(self, data, engine, session):
        metadata = db.MetaData()
        table = db.Table('err_news_orig', metadata, autoload=True, autoload_with=engine)
        article = Article(
            err_id = data['errID'],
            author = data['author'],
            related_urls = data['same_topic_urls'],
            url = data['url'],
            lead = data['lead'],
            content = data['content'],
            category = data['category'],
            date_published = data['datePublished'],
            editor = data['editor'],
            headline = data['headline'],
            language = data['language']
        )
        # Write only existing articles:
        article_written = [article.headline, article.lead, article.content]
        for a in article_written:
            if a is 'nullValue':
                return
            else:
                session.add(article)
                session.commit()

# article
Base = declarative_base()
class Article(Base):
    __tablename__ = 'err_news_orig'
    id = Column(Integer, Sequence('err_news_orig_id_seq'), primary_key=True)
    err_id = Column(Integer)
    author = Column(String)
    related_urls = Column(String)
    url = Column(String)
    lead = Column(String)
    content = Column(String)
    category = Column(String)
    date_published = Column(DateTime)
    editor = Column(String)
    headline = Column(String)
    language = Column(String)

    def __repr__(self):
        return "<Article(id='%s', headline='%s', err_id='%s')>" % (
                                self.id, self.headline, self.err_id)